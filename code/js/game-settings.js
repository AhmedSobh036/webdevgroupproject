'use-strict';

const startGameBtn = document.getElementById('start-button');
const submitButton = document.getElementById('submit-btn');

startGameBtn.disabled = true;
submitButton.disabled = true;

/**
 * @description
 * this function checks if the name is between 4 and 12 alphanumerals and that
 * the user has picked a color from the radio input as well as makes sure that the
 * user has chosen a difficulty otherwise the start game button will stay disabled
 * @returns this function returns a boolean that will be used else where that basically
 * says if the settings are valid or not (true/false)
 */
function validateSettings() {
  const userName = document.getElementById('player-name');
  const radios = document.querySelectorAll('input[type = radio]');
  if (!/^[a-zA-Z0-9]{4,12}$/.test(userName.value)) {
    userName.value = 'Name has to be between 4 and 12 alphanumerals';
    userName.style.border = '1px solid red';
    userName.style.backgroundColor = 'rgba(255, 0, 0, 0.4)';
    startGameBtn.disabled = true;
    return;
  }
  userName.style.border = '1px solid green';
  userName.style.backgroundColor = 'rgba(0, 255, 0, 0.4)';
  if (document.querySelector('select').value === '----') {
    startGameBtn.disabled = true;
    return;
  }
  if (document.getElementById('size').value > 6 || document.getElementById('size').value < 3) {
    startGameBtn.disabled = true;
    return;
  }
  for (let i = 0; i < radios.length; i++) {
    if (radios[i].checked) {
      startGameBtn.disabled = false;
    }
  }
}

/**
 * @description
 * this function makes sure that the user can only submit guesses when they have
 * selected the exact amount of tiles that they have to find
 */
function validateSubmit() {
  maxNumber = getMaxNum();
  if (document.getElementsByClassName('selectedTiles').length === maxNumber) {
    submitButton.disabled = false;
  } else {
    submitButton.disabled = true;
  }
}
