'use-strict';

const gamesInfoArray = [];
let loadGamesArray = [];
let clicksCount = 0;
const gameBoard = document.getElementById('tiles-area');
/**
 * this const is all the radio input buttons
 */
const radios = document.querySelectorAll('input[type = radio]');
let colorChosen;

const diff = document.querySelector('select');
diff.addEventListener('blur', validateSettings);
/**
 * @description
 * Here I create a constant containing all the input buttons
 * then i add an event listener to them all to validate the settings once
 * the user clicks off the input
 */
const allInput = document.querySelectorAll('input');
for (let i = 0; i < allInput.length; i++) {
  allInput[i].addEventListener('blur', validateSettings);
}
/**
 * @description
 * This loop adds an event listener to the radios which checks the settings and
 * through dynamic html changes the number of tiles that has to be selected
 * and the color of the words to the selected radio option
 */
for (let i = 0; i < radios.length; i++) {
  radios[i].addEventListener('click', displayNumAndColor);
  radios[i].addEventListener('blur', validateSettings);
}
/**
 * @description
 * This function checks the radio options to see which color was chosen then
 * determines how many tiles are that color
 *@returns the number of tiles to select according to the color chosen by the user
 */
function getMaxNum() {
  let maxNumber;
  for (let i = 0; i < radios.length; i++) {
    if (radios[i].checked) {
      colorChosen = radios[i].value;
      return document.getElementsByClassName(radios[i].value).length;
    }
  }
}
/**
 * @description
 * This function changes the number of tiles to be selected and the color of
 * the words to the selected color
 */
function displayNumAndColor() {
  const wordToChange = document.getElementById('change-with-colors');
  const numberToChange = document.getElementById('number-to-guess');
  for (let i = 0; i < radios.length; i++) {
    if (radios[i].checked) {
      wordToChange.style.backgroundColor = radios[i].value;
      wordToChange.innerText = radios[i].value;
      wordToChange.style.color = 'white';
      maxNumber = getMaxNum();
      if (maxNumber !== 0) {
        numberToChange.textContent = maxNumber;
      } else {
        numberToChange.textContent = 'number';
      }
    }
  }
}
/**
 * @description
 * This function has two purposes one is to count the number of clicks and the
 * second is to apply a border and a class on each tile that is selected
 * and remove the border and class if the tile was selected then pressed again
 */
function selected() {
  clicksCount++;
  const maxNum = getMaxNum();
  if (document.getElementsByClassName('selectedTiles').length >= maxNum && this.style.border === '') {
    alert('you have selected the max number of tiles either un-select to continue or submit your answer');
  } else if (this.style.border !== '') {
    this.style.border = '';
    this.classList.remove('selectedTiles');
  } else {
    this.style.border = '4px solid yellow';
    this.classList.add('selectedTiles');
  }
  validateSubmit();
}

/**
 * this loop adds an event listener to the radios which calls the function changeButtonsCollor
 */
for (let i = 0; i < radios.length; i++) {
  radios[i].addEventListener('click', changeButtonsColor);
}

/**
 * Here i create a constant which contains all the buttons on the webpage
 */
const allButtons = document.querySelectorAll('button');
/**
 * the function finds what color has beed selected from the radio inputs
 * then changes all buttons on the webpage to that color
 */
function changeButtonsColor() {
  const radioOptionSelected = document.querySelector('input[type = radio]:checked');
  const colorSelected = radioOptionSelected.value;
  for (let i = 0; i < allButtons.length; i++) {
    allButtons[i].style.color = 'white';
    allButtons[i].style.backgroundColor = colorSelected;
  }
}

/**
 * @description
 * This function is used to disable the nav section of the webpage when the
 * start game button is hit and it reactivate it when teh submit game session
 * button is hit so that the users cant change any information mid game
 */
function disableNav() {
  for (let i = 0; i < allInput.length; i++) {
    if (allInput[i].disabled) {
      allInput[i].disabled = false;
    } else {
      allInput[i].disabled = true;
    }
  }
  if (diff.disabled) {
    diff.disabled = false;
  } else {
    diff.disabled = true;
  }
  allInput[5].disabled = false;
}

/**
 * @description
 * Here i create a constatnt for the strat game button and i add an event listener
 * to generate the randomized tiles when clicked it also calls the disable nav function
 */
const startButton = document.getElementById('start-button');
startButton.addEventListener('click', () => {
  generateGame('div', gameBoard, size);
  disableNav();
  startButton.disabled = true;
});

/**
 * @description
 * here i create a constant cheat button and i add an event listener
 * to it which class the function to display the rgb values of each tile when
 * clicked and remove them when clicked again
 */
const cheatButton = document.getElementById('cheat-btn');
cheatButton.addEventListener('click', cheat);
function cheat() {
  const tiles = document.querySelectorAll('.gameTiles div');
  if (cheatButton.checked === true) {
    for (let i = 0; i < tiles.length; i++) {
      tiles[i].style.visibility = 'visible';
    }
  } else {
    for (let i = 0; i < tiles.length; i++) {
      tiles[i].style.visibility = 'hidden';
    }
  }
}

/**
 * This function is called when the user submits thier game and when the user cant
 * select any tiles anymore, so when the game is over the rgb values of all the tiles
 * are revealed
 */
function displayRGB() {
  const tiles = document.querySelectorAll('.gameTiles div');
  for (let i = 0; i < tiles.length; i++) {
    if (tiles[i].style.visibility === 'hidden') {
      tiles[i].style.visibility = 'visible';
    } else {
      tiles[i].style.visibility = 'hidden';
    }
  }
}

/**
 * @description
 *This function gets all the options chosen by the user like difficulty and size of the
 * size and difficulty of the game and calls the generate tiles function
 * to create each individual tile
 * @param {HTMLElement} tag
 * @param {HTMLElement} parent
 */
function generateGame(tag, parent) {
  while (gameBoard.children[0] != null) {
    gameBoard.removeChild(gameBoard.children[0]);
  }
  const difficulty = document.querySelector('select').value;
  const size = document.getElementById('size').value;
  for (let i = 0; i < size; i++) {
    const row = document.createElement(tag);
    parent.append(row);
    for (let j = 0; j < size; j++) {
      createTile('div', row, difficulty);
    }
  }
  const tiles = document.querySelectorAll('.gameTiles');
  for (let i = 0; i < tiles.length; i++) {
    tiles[i].addEventListener('click', selected);
  }
  displayNumAndColor();
  cheat();
}

/**
 * @description
 * this function disables all the tiles fromo being clicked when the user has
 * submitted his game result
 */
function disableTiles() {
  const tiles = document.querySelectorAll('.gameTiles');
  for (let i = 0; i < tiles.length; i++) {
    tiles[i].removeEventListener('click', selected);
  }
}

/**
 * @description
 * this function creats a div and appends it to the parent provided then according
 * to the difficulty which is passed in determines the range in which all values
 * of the rgb has to be within this function also adds the randomly
 * generated background color to the div and gives the tile a class which indicates
 * what is the most promenant color in the rgb
 * @param {HTMLElement} tag
 * @param {HTMLElement} parent
 * @param {any} difficulty
 * @returns HTMLElement this function returns each individual tile to then append
 * it to the parent div
 */
function createTile(tag, parent, difficulty) {
  const e = document.createElement(tag);
  let r;
  let g;
  let b;
  let color;
  let difference;
  let highestColor;
  if (difficulty === 'Easy') {
    r = Math.floor(Math.random() * 255);
    g = Math.floor(Math.random() * 255);
    b = Math.floor(Math.random() * 255);
  }
  if (difficulty === 'Medium') {
    difference = 80;
    r = Math.floor(Math.random() * 255);
    g = getRandomColor(difference, r);
    b = getRandomColor(difference, r);
  }
  if (difficulty === 'Difficult') {
    difference = 50;
    r = Math.floor(Math.random() * 255);
    g = getRandomColor(difference, r);
    b = getRandomColor(difference, r);
  }
  if (difficulty === 'Brutal') {
    difference = 10;
    r = Math.floor(Math.random() * 255);
    g = getRandomColor(difference, r);
    b = getRandomColor(difference, r);
  }
  color = `rgb(${r},${g},${b})`;
  const tileColor = createMyElement('div', e, color);
  tileColor.style.visibility = 'hidden';
  e.style.backgroundColor = color;
  highestColor = getHighestRGB(r, g, b);
  e.classList.add(highestColor);
  e.classList.add('gameTiles');
  parent.append(e);
  return e;
}

/**
 * @discription
 * This function takes in the difference that has to be between all three values
 * r, g and b and takes in the r which is randomly generated then generates according
 * to that value the value of the new randomly generated color
 * @param {any} difference
 * @param {any} r
 * @returns the randomly generated color
 */
function getRandomColor(difference, r) {
  const randomDifference = Math.floor(Math.random() * difference);
  const addOrSubtract = Math.floor(Math.random() * 2);
  if (addOrSubtract === 0) {
    c = r + Math.round(randomDifference / 2);
    if (c > 255) {
      g = r - Math.round(randomDifference / 2);
    }
  } else {
    c = r - Math.round(randomDifference / 2);
    if (c < 0) {
      c = r + Math.round(randomDifference / 2);
    }
  }
  return c;
}

/**
 * @description
 * this function takes in all three rbg values of a tile and returns the highest color
 * so that we can add a class to the tile that can identify it as its dominant color
 * @param {number} r
 * @param {number} g
 * @param {numer} b
 * @returns the value of the color that is most prominent in the randomly generated color
 */
function getHighestRGB(r, g, b) {
  if (r > b && r > g) {
    return 'red';
  } if (b > r && b > g) {
    return 'blue';
  } if (g > r && g > b) {
    return 'green';
  }
  if (r === b && r > g) {
    if (colorChosen === 'red') {
      return 'red';
    }
    return 'blue';
  }
  if (g === r && g > b) {
    if (colorChosen === 'green') {
      return 'green';
    }
    return 'red';
  }
  if (g === b && g > r) {
    if (colorChosen === 'green') {
      return 'green';
    }
    return 'blue';
  }
  if (g === b && g === r) {
    return colorChosen;
  }
}

/**
 *  @description
 * getSuccessPercentage
 * Finds the percentage of correct tiles were selected compared to the
 * number of tiles of that color were on the board in total
  */
function getSuccessPercentage() {
  let successPercentage;
  let numCorrectTiles = 0;
  const maxNum = document.querySelectorAll('span')[2].innerHTML;
  const colorToGuess = document.querySelectorAll('span')[1].innerText;
  const selectedTilesList = document.getElementsByClassName('selectedTiles');
  for (let i = 0; i < selectedTilesList.length; i++) {
    if (selectedTilesList[i].classList[0] === colorToGuess) {
      numCorrectTiles++;
    }
  }

  successPercentage = (numCorrectTiles / maxNum) * 100;

  return successPercentage.toFixed(2);
}

const submitBtn = document.getElementById('submit-btn');
submitBtn.addEventListener('click', () => {
  startButton.disabled = false;
  disableNav();
  displayRGB();
  disableTiles();
  submitBtn.disabled = true;
  const gameInfoObj = {
    player: document.getElementById('player-name').value,
    dur: 'NA',
    clicks: clicksCount,
    level: document.querySelector('select').value,
    success: getSuccessPercentage(),
    rowXcol: document.getElementById('size').value,
  };

  gamesInfoArray.push(gameInfoObj);

  for (let i = 0; i < gamesInfoArray.length; i++) {
    if (i === gamesInfoArray.length - 1) {
      generateTableEntriesFromObject(gamesInfoArray[i]);
      loadGamesArray.push(gamesInfoArray[i]);
    }
  }

  clicksCount = 0;

  validateLocalStorageBtns('save-game');

  // Validation for load btn
  const localStorageArray = localStorage.getItem('arrayGames');
  if (localStorageArray === 'null') {
    loadBtn.disabled = true;
  } else {
    loadBtn.disabled = false;
  }
});
