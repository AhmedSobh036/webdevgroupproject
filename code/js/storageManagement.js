'use-strict';

// Save btn handler and code for local storage
let savebtn = document.getElementById('save-game');
validateLocalStorageBtns('save-game');
savebtn.addEventListener('click', () => {
  if (document.querySelectorAll('tr')[1] == null) {
    alert('There are no game sessions to save to local storage...');
  }
  let stringedGameArray = JSON.stringify(loadGamesArray);
  localStorage.setItem('arrayGames', stringedGameArray);
  alert('This collection of games have been sent to the local storage!!!');

  let loadBtn = document.getElementById('load-game');
  let localStorageArray = localStorage.getItem('arrayGames');
  if (localStorageArray === 'null') {
    loadBtn.disabled = true;
  } else {
    loadBtn.disabled = false;
  }
});

/**
 *  @description
 * readAllGamesFromStorage -
 * gets local storage array, parses it and creates a table based on that parsed array
  */
function readAllGamesFromStorage() {
  let localStorageArray = localStorage.getItem('arrayGames');
  loadGamesArray = JSON.parse(localStorageArray);
  generateTableEntriesFromArrayObjects(loadGamesArray);
}

const loadBtn = document.getElementById('load-game');
let localStorageArray = localStorage.getItem('arrayGames');
if (localStorageArray === 'null') {
  loadBtn.disabled = true;
} else {
  loadBtn.disabled = false;
}
loadBtn.addEventListener('click', () => {
  alert("You're games are being loaded to the table right now!!!");
  deleteRows();
  readAllGamesFromStorage();
  validateLocalStorageBtns('save-game');
});

let clearBtn = document.getElementById('clear');
clearBtn.addEventListener('click', () => {
  alert('all game sessions have been removed from local storage');
  localStorage.clear();
  loadBtn.disabled = true;
});

/**
 *  @description
 * validateLocalStorageBtns
 * Checks to see if a table exists and sets the btn given
 * to be disabled or not based on the td/tr or not
  *
  * @param  {string} btnID
  */
function validateLocalStorageBtns(btnID) {
  let btnToValidate = document.getElementById(btnID);
  if (document.querySelectorAll('tr')[1] == null) {
    btnToValidate.disabled = true;
  } else {
    btnToValidate.disabled = false;
  }
}
