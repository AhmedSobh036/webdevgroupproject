'use-strict';

/**
 *  @description
 * createMyElement -
 * . create a dynamic HTMLElement, set its properties
 * . and attach it to the parent (HTMLElement)
 * . The properties (rest parameter- check labs/notes):
 *      . must have at least id, type,
 *      . optionally text content, value, class name, min, max
  *
  * @param {HTMLElement} tag
  * @param {HTMLElement} parent
  * @param  {...any} properities
 * @returns {HTMLElement} / return the created element
  */
function createMyElement(tag, parent, properties) {
  const newElem = document.createElement(tag);
  newElem.textContent = properties;
  parent.appendChild(newElem);
  return newElem;
}

/**
 *  @description
 * generateTableEntriesFromArrayObjects -
 * loops through given array and adds a row filled with tds based on the elements of the array
  *
  * @param  {array} ArrayObjects
  */
function generateTableEntriesFromArrayObjects(ArrayObjects) {
  const table = document.getElementById('table-id');
  ArrayObjects.forEach((obj) => {
    let entryRow = createMyElement('tr', table, null);
    for (item in obj) {
      createMyElement('td', entryRow, obj[item]);
    }
  });
}

/**
 *  @description
 * generateTableEntriesFromObjects -
 * loops through given object and adds a row filled with tds based on the elements of the object
  *
  * @param  {array} object
  */
function generateTableEntriesFromObject(object) {
  const table = document.getElementById('table-id');
  const entryRow = createMyElement('tr', table, null);
  for (item in object) {
    const td = createMyElement('td', entryRow, object[item]);
  }
}

// sorting tables code

/**
 *  @description
 * deleteRows -
 * loops through all rows aside from thead row and deletes them
  *
  * @param  {array} ArrayObjects
  */
function deleteRows() {
  trList = document.querySelectorAll('tr');
  for (let i = 1; i < trList.length; i++) { trList[i].remove(); }
}

/**
 *  @description
 * sortClicks -
 * Sorts the click column of entries of the array
  *
  * @param {number} num1
  * @param {number} num2
  */
function sortClicks(num1, num2) {
  if (Number(num1.clicks) < Number(num2.clicks)) {
    if (thArray[2].classList.contains('reverse')) {
      return 1;
    }

    return -1;
  }
  if (Number(num1.clicks) > Number(num2.clicks)) {
    if (thArray[2].classList.contains('reverse')) {
      return -1;
    }

    return 1;
  }
  if (Number(num1.clicks) === Number(num2.clicks)) {
    return 0;
  }
}

/**
 *  @description
 * sortSucceed -
 * Sorts the succeed column of entries of the array
  *
  * @param {number} num1
  * @param {number} num2
  */
function sortSucceed(num1, num2) {
  if (Number(num1.success) < Number(num2.success)) {
    if (thArray[4].classList.contains('reverse')) {
      return 1;
    }

    return -1;
  }
  if (Number(num1.success) > Number(num2.success)) {
    if (thArray[4].classList.contains('reverse')) {
      return -1;
    }

    return 1;
  }
  if (Number(num1.success) === Number(num2.success)) {
    return 0;
  }
}

/**
 *  @description
 * sortRowXCol -
 * Sorts the rowXcol column of entries of the array
  *
  * @param {number} num1
  * @param {number} num2
  */
function sortRowXCol(num1, num2) {
  if (Number(num1.rowXcol) < Number(num2.rowXcol)) {
    if (thArray[5].classList.contains('reverse')) {
      return 1;
    }

    return -1;
  }
  if (Number(num1.rowXcol) > Number(num2.rowXcol)) {
    if (thArray[5].classList.contains('reverse')) {
      return -1;
    }

    return 1;
  }
  if (Number(num1.rowXcol) === Number(num2.rowXcol)) {
    return 0;
  }
}

/**
 *  @description
 * sortLevels -
 * Sorts the level column of entries of the array
  *
  * @param {number} num1
  * @param {number} num2
  */
function sortLevels() {
  let diff = [{ difficulty: 'easy', num: 1 }, { difficulty: 'medium', num: 2 }, { difficulty: 'difficult', num: 3 }];
}

const thArray = document.querySelectorAll('th');

thArray[0].addEventListener('click', () => {
  loadGamesArray.sort((player1, player2) => {
    const name1 = player1.player.toLowerCase();
    const name2 = player2.player.toLowerCase();

    if (name1 < name2) {
      if (thArray[0].classList.contains('reverse')) {
        return 1;
      }

      return -1;
    }
    if (name1 > name2) {
      if (thArray[0].classList.contains('reverse')) {
        return -1;
      }

      return 1;
    }
    if (name1 === name2) {
      return 0;
    }
  });
  deleteRows();
  console.log(loadGamesArray);
  console.log(gamesInfoArray);
  generateTableEntriesFromArrayObjects(loadGamesArray);
  if (thArray[0].classList.contains('reverse')) {
    thArray[0].classList.remove('reverse');
  } else {
    thArray[0].classList.add('reverse');
  }
});

thArray[2].addEventListener('click', () => {
  loadGamesArray.sort((num1, num2) => sortClicks(num1, num2));
  deleteRows();
  generateTableEntriesFromArrayObjects(loadGamesArray);
  if (thArray[2].classList.contains('reverse')) {
    thArray[2].classList.remove('reverse');
  } else {
    thArray[2].classList.add('reverse');
  }
});

thArray[3].addEventListener('click', () => {
  // localStorageArray = localStorage.getItem('arrayGames');
  // loadGamesArray = JSON.parse(localStorageArray);
  // loadGamesArray.sort((num1, num2) =>
  // Call sort function here
  // deleteRows();
  // generateTableEntriesFromArrayObjects(loadGamesArray);
  // thArray[2].addEventListener('click',() => {
  //     call sort function (descending) here
  //         deleteRows();
  //         generateTableEntriesFromArrayObjects(loadGamesArray);
  // })
});

thArray[4].addEventListener('click', () => {
  loadGamesArray.sort((num1, num2) => sortSucceed(num1, num2));
  deleteRows();
  generateTableEntriesFromArrayObjects(loadGamesArray);
  if (thArray[4].classList.contains('reverse')) {
    thArray[4].classList.remove('reverse');
  } else {
    thArray[4].classList.add('reverse');
  }
});
thArray[5].addEventListener('click', () => {
  loadGamesArray.sort((x, y) => sortRowXCol(x, y));
  deleteRows();
  generateTableEntriesFromArrayObjects(loadGamesArray);
  if (thArray[5].classList.contains('reverse')) {
    thArray[5].classList.remove('reverse');
  } else {
    thArray[5].classList.add('reverse');
  }
});
